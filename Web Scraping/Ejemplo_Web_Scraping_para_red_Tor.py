# Ejemplo de Script en Python para Web Scraping en red Tor usando BeautifulSoup

_author__ = 'Facundo Gallo'

import socks5
import socks #Para usar esta librería hace falta instalar Pysocks y Pysock5
import socket
import requests
from bs4 import BeautifulSoup
import re
import csv
import time # Se activará si quiero ponerle el Timestamp
import sys
import os

# Configuro Socks para usar Tor

from urllib.request import urlopen

socks.set_default_proxy(socks.SOCKS5, "localhost", 9150)
socket.socket = socks.socksocket


# Necesario para la resolución de DNS en Tor
def getaddrinfo(*args):
    return [(socket.AF_INET, socket.SOCK_STREAM, 6, '', (args[0], args[1]))]

socket.getaddrinfo = getaddrinfo

# Leemos la pagina Stronghold Paste
res = requests.get("http://strongerw2ise74v3duebgsvug4mehyhlpa7f6kfwnas7zofs3kov7yd.onion/all")
# Link funcional hasta el 2021: http://nzxj65x32vh2fkhk.onion/all

# Transformo el contenido con beatifulsoup
soup = BeautifulSoup(res.content, 'html.parser')

# Obtengo el titulo
soup.title

# Obtengo lo que este en la clase donde se escribe el contenido
eq = soup.find_all('div', class_='well well-sm well-white pre')

# Me creo una lista para cada entrada
entradas = list()

count = 0

# Recorro un bucle para guardar el contenido hasta 20 entradas
for i in eq:
    # nos quedamos con los 20 primeros:
    if count < 20:
        entradas.append(i.text)
    else:
        # si no es menor a 20 salir del bucle
        break
        # vamos incrementando la lista
    count += 1
print(entradas, end='\n')

# Convierto la lista a String por si necesito recuperar en este formato
# cadena = " ".join(entradas)

# Hago limpieza del fichero csv
if os.path.exists("../Archives/StrongholdPaste.csv"):
    os.remove("../Archives/StrongholdPaste.csv")

# Guardo los resultados en un CSV codificado en utf-8
mi_path = '../Archives/StrongholdPaste.csv'
f = open(mi_path, 'a+', encoding='utf-8')

# Recorro la lista entradas para escribir el CSV
for i in entradas:
    # Me creo una variable now que capture la hora del PC como timestamp y la imprimo en cada linea
    # now = time.strftime('%d-%m-%Y %H:%M:%S')
    # f.write(now)
    f.write('')
    f.write(i)
    f.write(';')
f.close()
