# Instalar Nodo y Servicio Oculto en la red Tor
Los pasos indicados a continuación han sido probados bajo un entorno Debian 4.19.181-1 (2021-03-19).

**1** Buscamos actualizaciones:
`apt update`

**2** Instalamos Nginx
`apt-get install nginx`

**3** Iniciamos el servicio de Nginx
`service nginx start`

**4** Comprobamos su estado por si existieran errores:
`service nginx status`

**5** Configuramos Nginx para que solo pueda escuchar conexiones localhost, para esto vamos a la ruta:
`/etc/nginx/sites-enabled`
y modificamos el archivo `default`:
En la linea 17, donde pone "server" hay que especificar: `listen 127.0.0.1:80`y borrar lo que queda hasta la línea "19" (SSL)

**6** Reiniciamos el servidor nginx para aplicar los anteriores cambios:
`service nginx restart`

**7** Si hacemos un: curl 127.0.0.1 podremos ver ahora el contenido web genérico de Nginx (instalar curl en caso de no contar con este paquete)

**8** Vamos a la carpeta /var/www/html y creamos un fichero index.hml de manera provisional con la siguiente estructura.
<html>
<body>
hola mundo
</body>
</html>

**9** En caso de contar con un firewall, permitimos el tráfico HTTP:
`ufw allow 'Nginx HTTP'`

**10** Instalamos el servicio Tor
`apt install tor`

Tras la instalación probamos que funcione correctamente:
`service tor status`

**11** Localizamos el fichero _torrc_ presente en la ruta _/etc/tor_ y descomentamos las siguientes líneas presentes en el apartado > This section is just for location-hidden services:

```
HiddenServiceDir /var/lib/tor/hidden_service/
HiddenServicePort 80 127.0.0.1:80
```

**12** Reiniciamos tor:
`systemctl restart tor`

**13** Accedemos a la ruta:
`/var/lib/tor` 

**14** Aquí debemos encontrar el siguiente directorio generado tras el reinicio de Tor:
_hidden_service_

**15** Dentro de este directorio tendremos un fichero _hostname_ dentro del cual se encuentra la dirección ".onion" auto-generada.

**16** Para desplegar el servicio oculto es recomendable tener una versión de la web en Git y clonar dentro del directorio de _/var/www/html_ el contenido de la web, para ello instalamos Git:
`apt-get install git`
y luego clonamos el sitio web desde el repositorio:
`git clone https://github.com/ruta_del_directorio`

Referencias:
(1) https://2019.www.torproject.org/docs/debian.html.en
